# Contributing

If you consider contributing to this project, please discuss the changes or
improvements that you would like to make via e-mail to philippe.marbaix -at-
uclouvain.be or by opening an issue within framagit.org. 

Thanks for your interest!

