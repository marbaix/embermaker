# The EmberFactory and related "IPCC-like burning embers" code was first drafted by Philippe Marbaix
# (philippe.marbaix@uclouvain.be) in 2020 (see Zommers et al. 2020, NREE).
# The code of all versions is open-source (GPL-3).
# As of 08/2023, this project did not receive any official funding and was not part of any paid duties
# (that is: not part of any duty other than the moral obligation of maintaining a tool that has been used to illustrate
#  scientific findings and now ensures replicability of several illustrations).
