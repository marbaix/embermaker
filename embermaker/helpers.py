import numpy as np
import logging
from sys import gettrace
from defusedxml.common import EntitiesForbidden
from openpyxl import load_workbook
from openpyxl import DEFUSEDXML
import zipfile
from pdf2image import convert_from_path, convert_from_bytes
from pdf2image.exceptions import PopplerNotInstalledError
from os import path
from io import BytesIO
"""
This module contains several types of "helper functions" :
- nicelevels: devides a range of value into a set of well-chosen levels for graphics (e.g. axis)
- logging functions mostly aimed at facilitating the reporting of errors into the web interface
- opening xml files safely using defusexml and also checking the uncompressed size.
- relatively basic 'cleaning' of strings from input data prior to use
- higher level functions to facilitate the drawing of strings in Reportlab's canevas

05/2020: The logging part is adapted from an earlier version of the code, and may not be fully appropriate
and/or well structured. It works and remains needed here, but may change substantially or be removed in the future.
"""

# For protect against XML bombs, defusedxml must be installed; it will then be used automatically by openpyxl.
# https://pypi.org/project/defusedxml/
if not DEFUSEDXML:
    logging.warning("Defusedxml does not work! It is probably missing from your installation.")


def nicelevels(vbot: float, vtop: float, nalevels: int = 5, nmticks: int = None, enclose: bool = False) -> tuple:
    """
    Finds a nice value for the step to devide a range of values in nice levels, given constraints

    :param vbot: the start of the interval  to be represented
    :param vtop: the end of the interval to be represented (vtop does not need to be > vbot)
    :param nalevels: approximative numbers of desired levels (divisions) between min and max
    :param nmticks: number of minor ticks; default is no minor ticks, 0 means auto selection
    :param enclose: whether the levels should enclose the range or be within the range (default)
    :return: levels = selected levels so that intervals are [ 5., 2.5, 2.0, 1.0]  *  1.E exponent (order: increasing)
             labfmt = formatting string for the axis labels
             mticks = suggested number of minor tick marks between two main tick marks
    """
    if vbot > vtop:
        vtop, vbot = (vbot, vtop)

    #  Possible values for auto-set step between levels and corresponding prefered number of minor ticks:
    #   -first element of each tuple is the step (significand part, decreasing order),
    #   -second element is the number of minor ticks
    aticks = [(5.0, 5.0), (2.5, 5.0), (2.0, 4.0), (1.0, 5.0)]
    #
    nalevels = max(3, nalevels)

    #  First approximation of the step :
    astep = (vtop - vbot) / (float(nalevels - 1) * 0.8)
    if np.abs(astep) <= 1.E-30:
        astep = 1.E-30

    #  Exponent:
    estep = np.floor(np.log10(astep) + 99.) - 99.
    #  Significand part of the step (mantissa, see https://en.wikipedia.org/wiki/Significand):
    mastep = astep / 10 ** estep
    #  Step selection:
    mstep, mticks = [tick for tick in aticks if tick[0] <= mastep][0]
    stp = (10 ** estep) * mstep  # interval between main ticks
    if nmticks is None:
        mstp = stp
    else:
        if nmticks == 0:
            mstp = stp / mticks  # interval between minor ticks
        else:
            mstp = stp / nmticks
    #
    #  Formatting string for labels
    #  Potential strings:
    lab = ["6f", "5f", "4f", "3f", "2f", "1f", "0f"]
    ilab = int(round(estep))
    if (ilab <= 5) and (ilab > 0):
        labfmt = "0f"
    elif (ilab >= -5) and (ilab <= 0):
        labfmt = lab[ilab + 6 - (mstep == 2.5)]
    else:
        labfmt = "2e"

    # Generate levels (main ticks)
    op = np.floor if enclose else np.ceil
    newmin = op(vbot / stp) * stp
    nlevels = np.ceil((vtop - newmin + stp * 0.01) / stp) + enclose
    nlevels = max(2, nlevels)
    levels = np.arange(nlevels) * stp + newmin

    # Generate minor ticks
    if nmticks is None:
        mticks = None
    else:
        mtmin = np.ceil((min(vbot, newmin)) / mstp) * mstp  # first minor tick
        tmticks = np.ceil((vtop - mtmin + mstp * 0.01) / mstp)  # total number of ticks (minor+main)
        # Get minor ticks as all ticks - main ticks (remove tick if = main tick)
        mticks = [tick for tick in np.arange(tmticks) * mstp + mtmin if tick % stp != 0.0]

    return levels, labfmt, mticks


class Logger:
    """
    Logging mechanism for the EmberFactory. Collects processing messages to inform users.
    Main levels are standard: INFO, WARN, CRITICAL (generated by addfail()).
    Note that the origin of this code is quite old; it might make better use
    of standard python logging in the future.
    """
    # Opening and closing html tags to return messages as html
    mtags = {20: ('<p class="compact indent">', '</p>'),  # Information text
             24: ('<h4>', '</h4>'),  # Information text title
             25: ('<h5>', '</h5>'),  # Information text subtitle
             30: ('<p class="error">', '</p>'),  # css class names aren't well mapped > to improve!
             40: ('<p class="critical">', '</p>'),
             50: ('<p class="critical">', '</p>'),
             }

    def __init__(self):
        if gettrace():
            logging.basicConfig(level=logging.DEBUG)
        self.logmes = []  # list of tuples containing (log_message, log_level)
        # else - we may limit the storage of log message to "CRITICAL" when not in debug mode?

    def addinfo(self, mes, mestype=None):
        """
        Adds an information message to the log
        :param mes:
        :param mestype: the kind of message, for messages such as the starting of a section => formatted as a title
        :return:
        """
        logging.debug(mes)
        fmtmes = rembrackets(mes)
        if mestype == 'title':
            itype = 24
        elif mestype == 'subtitle':
            itype = 25
        else:
            itype = 20
        self.logmes.append((fmtmes, itype))  # Following the usual python convention for logging, 20 is "info"

    def addwarn(self, mes, severe=False):
        """
        Logs a warning message. If 'severe' is True, it logs a Python 'error' (level = 40)
        :param mes:
        :param severe: Whether the warning is "severe"
        :return:
        """
        if severe:
            logging.error(mes)
            level = 40  # Following the usual python convention for logging, 40 is "ERROR"
            fmtmes = 'SEVERE ISSUE: ' + rembrackets(mes)
        else:
            logging.warning(mes)
            level = 30  # Following the usual python convention for logging, 30 is "WARN"
            fmtmes = rembrackets(mes)
        self.logmes.append((fmtmes, level))

    def addfail(self, mes):
        """
        Log message as critical error and returns it (for further processing within the function termination)
        This should imply that the program cannot continue.
        """
        mes = "CRITICAL ERROR: " + mes
        logging.critical(mes)
        fmtmes = rembrackets(mes)
        self.logmes.append((fmtmes, 50))  # Following the usual python convention for logging, 50 is "CRITICAL"
        return mes

    def getlog(self, levelname, only=False, as_html=False):
        if type(levelname) is int:
            intlevel = levelname
        else:
            intlevel = logging.getLevelName(levelname.upper())
        if type(intlevel) is not int:
            return [f'Unknown logging level name "{levelname}"']

        if only:  # Do not get higher level messages (ignore 'scarier' messages), to isolate message levels
            maxlevel = intlevel
        else:
            maxlevel = 100

        try:
            if as_html:
                # Add opening and closing html tags, as defined in mtags and required by error level:
                mes = [self.mtags[lm[1]][0] + lm[0].replace("\n", "<br >") + self.mtags[lm[1]][1]
                       for lm in self.logmes if intlevel <= lm[1] <= maxlevel]
            else:
                mes = [lm[0] for lm in self.logmes if intlevel <= lm[1] <= maxlevel]
        except (KeyError, NameError):
            mes = ['EmberMaker logging failed']
        return mes


def rembrackets(instr):
    # This was mostly useful when the code run with mod_python,
    # as Flask probably does it by default. Consider removing.
    return str(instr).replace('<', '&lt;').replace('>', '&gt;')


def secure_open_workbook(file, maxsize=8E6):
    # Attempt at protecting against a real or poential (?) zip bomb:
    # (is it really needed? not sure - see here: https://bugs.python.org/issue36260`)

    try:
        with zipfile.ZipFile(file) as zf:
            if sum(zi.file_size for zi in zf.infolist()) > maxsize:
                raise Exception('Excel file appears too large for reading; please contact app managers')
    except zipfile.BadZipFile:
        raise ValueError('Excel file appears invalid [Z]')

    try:
        return load_workbook(file, read_only=True, data_only=True)
    except EntitiesForbidden:
        raise ValueError('Excel file appears invalid [L]')


def isempty(value):
    """
    Finds if a variable is "empty", whatever the type and including strings containing blanks.
    In particular, isempty([]) is True, but isempty(0) is False (!).
    :param value: the data to be checked
    :return: whether the input value is judged 'empty'
    """
    if value is None or (hasattr(value, "__getitem__") and len(value) == 0):
        return True
    if isinstance(value, str):
        if not value.strip():
            return True
    return False


def hasindex(lst, index):
    """
    Returns True if lst is a list, and lst(index) exists and contains something (including 0 and '')
    :param lst:
    :param index:
    :return:
    """
    return isinstance(lst, list) and index < len(lst) and (lst[index] is not None)


def stripped(value, default=None):
    """
    Remove blanks if the value is a string, don't touch if it is not a string,
    except if it is None: in that case replace by the default value when provided.
    :param value: the string to be processed
    :param default: to be returned if value is None
    :return: the 'stripped' string
    """
    if isinstance(value, str):
        return value.strip()
    elif value is None and default is not None:
        return default
    else:
        return value


def norm(value):
    """
    Convert to a string if needed, then "normalise" to help with comparisons:
    remove the blanks at both ends and make lower case.
    Works on scalars and lists. Does not touch values which are not strings, including None

    :param value: the value or list of values to be processed
    :return: the 'normalised' string
    """
    if isinstance(value, list):
        return [str(el).strip().lower() if isinstance(el, str) else el for el in value]
    else:
        return str(value).strip().lower() if isinstance(value, str) else value


def htmlbreaks(value):
    return value.replace("\r\n", "\n").replace("\r", "\n").replace("\n", "<br>")


def rasterize(inputpdf, outfilext, fmt='png', width=800, dpi=200, jpegopt=None, cairo=True, logger=None):
    """
    Converts PDF files to image formats through pdf2image.

    :param inputpdf: either a file path as string or a BytesIO (stream) object
    :param outfilext:
    :param fmt:
    :param width:
    :param dpi:
    :param jpegopt:
    :param cairo:
    :param logger: an optional instance of Logger, which would contain the logging messages.
    :return: output file path
    """
    # :Future changes: raise exceptions if it fails, instead of reporting None + warning?
    if logger is None:
        logger = Logger()
    if width:
        size = (width, None)
    else:
        size = None
    result = None  # In case of failure, when no image was produced
    try:
        if type(inputpdf) is BytesIO:
            img = convert_from_bytes(inputpdf.getvalue(), fmt=fmt, size=size, dpi=dpi,
                                        jpegopt=jpegopt, use_pdftocairo=cairo)
            inputpdf.close()
            if not img or len(img) < 1:
                logger.addwarn("Conversion from PDF to image failed (did not get image).")
                return None
            result = BytesIO()
            img[0].save(result, format="PNG")
        elif type(inputpdf) is str:
            output_folder = path.dirname(inputpdf)
            output_file = path.splitext(outfilext)[0]
            if convert_from_path(inputpdf, output_folder=output_folder, single_file=True,
                                 output_file=output_file, fmt=fmt, size=size, dpi=dpi, jpegopt=jpegopt,
                                 use_pdftocairo=cairo):
                result = path.join(output_folder, outfilext)
            else:
                logger.addwarn("Conversion from PDF to image failed (did not get image).")
        else:
            logger.addwarn(f"Conversion from PDF to image failed: unknown input object ({type(inputpdf).__name__})")
            return None
    except PopplerNotInstalledError:
        logger.addwarn("To convert diagrams to image formats other than PDF, you need to install Poppler.")
    except RuntimeError:
        logger.addwarn("Conversion from PDF to image failed.")

    return result


def getpath_defaults():
    return path.join(path.dirname(__file__), "defaults/")
