# Changelog

## Potential future improvements
- For clarity and maintainability of the code, consider the following (made possible by the addition of Axis in 2024)
  - removing has_axis_name and has_axis2_name
  - refactoring (& improving) graphbase > draw_vaxis/xaxis as method(s) of Element or Axis
  - clarifying the axis names: replace the former vaxis by yaxis
- Add support for the additional axis in EmberFactory (axis2) through the new approach (Axis class instead of global parameters)
- Consider "auto" options for layout parameters: in particular, the "margins" of graphs would be adjusted to their
  content, specifically to avoid blank spaces on top of diagrams (see climrisk.org/cree: would that be useful?)
- Improve graphbase>draw_vaxis and the related drawaxis so that user-defined shades (= ranges) appear *below* the
  auto-defined axis. This is currently achieved with transparent shades; reordering layers would be an improvement.
- Document and further improve the API
- Rewrite Embergraph>ColourPalette entirely: it dates back to the early days of the EmberFactory;
  it is now inadequate for several reasons (see ColourPalette)  
- Update numpy to 3.x (= check that this library + the related projects are fully compatible)

## [2.1.1] - 2025

### Minor improvements
- Added new pre-defined colors for shaded areas (ranges).
- Added optional tooltips for Axis (feature requested for the CREE / Embers database)

## [2.1.0] - 2024-08

### Additions
- New Axis class, improving the API (reducing the need for changing global graphical parameters (gp))
- Each ember group or line plot may now have its own vertical axis, which can be defined through the API.
  The previous approach, which defines an axis within graphical parameters is used as a default for the entire diagram.
  When all groups in a line share the same axis, it is only shown once.
- Embers have a standard variable name (std_var) which must match the std_var of the EmberGroup; if it does not,
  a conversion is attempted.

### Improvements:
- Inverted axis are now allowed (haz_axis_top < haz_axis_bottom) [Experimental feature, only for development]

### Bug fixes:
- Four ember groups containing only 1 ember, the length of the title was incorrect (way too short)

## [2.0.1] - 2024-1
This minor update fixes details to better report errors and normal processing log information (especially within the
related "EmberFactory").

## [2.0.0] - 2023-12

First 'library' version, separated from the Ember Factory

- Confidence levels are now attached to each 'transition' instead of its 'levels'.
- The new optional parameter "conf_lines_ends" allows for more "precise" vertical lines highlighting the transitions,
  as an alternative to the standard "gap" used to separate a transition from the next. The new options indicate 
  the full length of each transition, as they do use symbols instead of a white gap to indicate the transition limits.
- Details were improved, such as for indicating "mixed confidence levels" (e.g. 'medium to high confidence') by
  coloring the last bullet in grey instead of using brackets (this is the new default).

## [1.0.0 - 1.8.2]

All versions < 2.0 were embedded in the EmberFactory: https://framagit.org/marbaix/ember-factory
