# EmberGraph development & design notes

This is a draft note written in 2023 to describe the logic underpinning the 'nested graphic elements' created
in EmberMaker 2.0: EmberGraph > GraphLine > (EmberGroup > Ember | LinePlot > Line). 
These elements are placed on the canvas following specific rules, with a logic sketched in 'EmberGraph_layout.svg'

**This note remains an early draft but it is provided with the expectation that is better than no introduction at all**

## Nested graphic elements

### Rationale

### Required methods and attributes for an element

A graphic element is a subclass of Element. The class must have the following attributes:

- _parent
  which indicates the name of the allowed parent element  (Except for the EmberGraph class, which is thte top-level element). 

- _elementary
  if True, the element cannot have child elements (for example, a line in a lineplot). By default, it is False.

And the following methods:

- add(element or list[elements])
  unless it is "elementary" (that is, it cannot contain nested elements, see above). This method adds a child elements.

- size_x(egr) => obsolete !?
  at present, it is assumed that all elements can return their size even before they are drawn.  By default, the size of an element is the sum of all the nested elements (however, the EF defines a "maximum line lenght" as a number of ember groups; this will need to be taken into account)

- set_cx(), set_cy() => sets a Coord object for x and y axis; 
  
  This object is defined at the level where it becomes relevant (e.g. cy is defined for a linegraph) and "propagated" to nested elements. 
  
- the Coord class provides 1-dimensional "coordinate systems":
   - information on the position of an element on the canvas,
   - conversion methods from data to canvas coordinates and vice-versa.
   - a reference to an Axis is provided in each Coord. Axis store the data-related information,
     including parameters about tick marks etc.
   (see graphbase.py)

- draw(egr, pos)
  draws the element itself and calls .draw() on its nested elements.

### Placement of elements on the canvas

At the start of a .draw() function, it must be possible to get the base point for drawing in this element, or the full bounding box of this element. Calculation can use specific data, that is, a position point or bounding box, and egr.gp parameters.

After drawing, an element may update its size, if that is needed - but it should be better avoided. 

## Processing sequence

The processing will follow the follwing steps:

- add => construct of set of graphical elements 
- attach => link the graphical elements to the embergraph AND manage the placement of those elements within the graph
- draw => actually draw the elements

### The `attach` sequence & the placement of elements

EmberGraph : manages the placement of GraphLines. As it is responsible for the placement, it also **asks each graphline to set its own y-axis** and return the position of the upper end, to use this as the starting point to ask the next graphline, etc. 

GraphLine: manages the placement of groups along the x-axis. 

Other elements: an element either asks its embedded elements to place themselves and return the new start point, or to propage the parent coordinates.

Vertical axis: it can be drawn within the line or within the group => complex?
