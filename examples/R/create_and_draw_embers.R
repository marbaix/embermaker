# Creating 'burning embers' with the EmberFactory from R
#
# This example shows how EmberMaker can be used in a script written in the R language (https://www.r-project.org)
# It works, but I am not actively using R at the moment: advice and feedback would be specifically welcome.
#
# As the equivalent python code and IPCC practice, this assumes that your data is split in "risk transitions";
#   if, by contrast, you have a single 'continuous' data table representing hazard (e.g. warming) = f(risk_index),
#   it is possible to split that in transitions; we may provide a function to help with this in an upcoming version,
#   please do not hesitate to tell us is it would be useful for you.
#
# philippe.marbaix@uclouvain.be - december 2023.

ember_name = "An ember from R drawn with EmberFactory"
# Let us assume we constructed some ember data within R (= as R objects, not Python's):
transition_mod_high <- list(min = 2.4, median = 3.2, max= 3.5)
confidence_mod_high <- 'high'

# Reticulate is the R package to access Python
library(reticulate)

# Install EmberMaker in python is not done already (this uses your default python, you may need to change it)
if (!py_module_available("embermaker")) {py_install("embermaker")}

# Create an ember object
embermodule <- import("embermaker.ember", conv=FALSE)
be <- embermodule$Ember(name=ember_name, haz_valid=list(0,5), haz_name_std='GMST')
# Note: haz_valid is mandatory, it indicates the range of hazard values within which the assessment was done.

# Add some transitions to the ember (be)
# The first one directly from numbers, to illustrate how it works:
be$trans_create(name='undetectable to moderate', min=0.6, median=1.1, max=1.4, confidence='very high')
# The second one from the values we have set in R above
# (!!! unpacks the key/values for min, median, etc., as ** in python; it needs Reticulate 1.29):
be$trans_create(name='moderate to high', confidence=confidence_mod_high, !!!transition_mod_high)

# Loads the default parameters - if you want to change some of the defaults, otherwise you can just ignore this
# (it seems easier or possibly needed to follow this approach when accessing from R, unlike in Python;
#  accessing later from the graph object may result in changing a copy of the parameters instead of the used object)
gpmodule <- import("embermaker.parameters", conv=FALSE)
gp <- gpmodule$ParamDict()

# Change parameters, if desired (the list of parameters is here: https://climrisk.org/emberfactory/doc/parameters)
py_set_item(gp, 'haz_axis_top', 5.0)

# Create an ember graph (remove gp=gp if it was not defined above)
graphmodule <- import("embermaker.embergraph")
egr <- graphmodule$EmberGraph('test_EmberMaker_from_R', gp=gp, grformat="SVG")

# Produce the diagram
egr$add(be)
egr$draw()