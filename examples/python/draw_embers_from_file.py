"""
Drawing groups of ember by taking data from 'standard format' Excel files
+ illustrates how a simple line-plot can be added, using the same y-axis.
"""
from embermaker.lineplot import LinePlot, Line
from embermaker.embergraph import EmberGraph
from embermaker.ember import getgroups
from embermaker.readembers import embers_from_xl
import embermaker.helpers as hlp
from os import path

embersfile = "../data/Standard-SRCCL-example.xlsx"

# Create an ember graph
outfile = 'examples/out/test'
egr = EmberGraph(outfile, grformat="SVG")
gp = egr.gp  # get access to the graphic parameters
# (for information on these parameters, see https://climrisk.org/emberfactory/doc/parameters)

# Read embers (might be further simplified)
infile = path.join(path.dirname(__file__), embersfile)
wbmain = hlp.secure_open_workbook(infile)
lbes = embers_from_xl(wbmain, gp=gp)  # Get the embers and update gp with the parameters in the data sheet
# Optionally read parameters from the "Graph parameters" sheet Excel WB, if there are any (= replace the default values)
gp.readparams(wbmain)

# Change parameters - if needed (further customisation, replacing what was read from files)
gp["max_gr_line"] = 10  # The default is 3 groups of embers or lineplots per line (here we want all 4 on one line)

# Group embers according to their group names:
gbes = getgroups(lbes)
# Add groups of embers (and the embers it contains), to the graph:
egr.add(gbes[0])

# Create a line plot
lp = LinePlot(egr, xaxis_name="Some X Axis", title="Dumb data for testing")
# ^^^ Equivalent to first creating lp = LinePlot(), then calling egr.add(lp)
lp.add(Line(xs=(0, 0.5, 0.2, 0, 0.15, 0), ys=range(6), color="blue"))
lp.add(Line(xs=(0.1, 0.4, 0.5, 0.3, 0.2, 0), ys=range(6), color="red"))

# Actually produce the diagram
outfile = egr.draw()

print(outfile)
