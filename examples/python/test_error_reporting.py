"""
Error-reporting example and test
"""
from embermaker.embergraph import EmberGraph
from embermaker import ember as emb
from embermaker.helpers import Logger

# Create ember
be = emb.Ember(name='Test data with voluntary mistakes', haz_valid=[0, 5], haz_name_std='GMST')

# Create transitions... making plenty of mistakes for test:

# p0 (percentile 0) is valid but not recommended (use 'min='), however there is no max => warning message:
be.trans_create(name='undetectable to moderate', p0=0.5, tmedian=1.0, confidence='low')

# In the next transition, there is no min: this generates a 'warning' because a transition needs a min and a max;
#    this alone is not a critical error as it "only" implies an incomplete transition, not an inconsistency;
#    *however*, it also generates a 'critical' error due to the absence of max in the previous transition AND min here:
#    this means that the "moderate risk" level is not defined at all, resulting in a "missing colour";
#    as a result, the colours in the ember would not 'match' the legend, which is a critical mistake:
be.trans_create(name='moderate to high', p33=2.5, p66=3.0, tmax=4.5, confidence='high')

# The absence of confidence level is not an error, and the median is not mandatory => no error here
be.trans_create(name='high to very high', tmin=4.5, tmax=5.0)

# To get access to the log message, create a specific logger
# (not mandatory: warnings and errors would go through standard python logging anyway)
logger = Logger()

# Create an ember graph
outfile = 'test'
egr = EmberGraph(outfile, grformat="PDF", logger=logger)
egr.gp['haz_axis_top'] = 5.0  # Change max on axis (example, see https://climrisk.org/emberfactory/doc/parameters)
egr.gp['leg_pos']="right"

# Add ember to named group, then to graph:
be.group = "The name of the test group"
egr.add(be)

# Actually produce the diagram
outfile = egr.draw()

print("All logged messages:")
for msg in logger.getlog("INFO"):
    print(f"  -{msg}")
