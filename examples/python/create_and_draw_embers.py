"""
Drawing embers by inserting data through API functions (= not by reading a file)

Note:
As the standard IPCC approach, this assumes that your data is split in "risk transitions";
  if, by contrast, you have a single 'continuous' data table representing hazard (e.g. warming) = f(risk_index),
  it is possible to split that in transitions; we may provide a function to help with this in an upcoming version,
  please do not hesitate to tell us is it would be useful for you
  (this was done in the earlier 'full flexibility' format)
"""
from embermaker.embergraph import EmberGraph
from embermaker import ember as emb

# Create ember
be = emb.Ember(name='Test - fake data', haz_valid=[0, 5])

# Add a first risk transition within this ember
be.trans_create(name='undetectable to moderate', tmin=0.6, tmedian=1.1, tmax=1.4, confidence='very high')
# Add a second transition, with more intermediate risk levels = "percentiles"
# (use with moderation = up to what can be assessed/meaningful !)
be.trans_create(name='moderate to high', tmin=2.0, p33=2.5, p66=3.0, tmax=3.7, confidence='medium')

# Create an ember graph
outfile = 'examples/out/test'
egr = EmberGraph(outfile, grformat="PDF")
egr.gp['haz_axis_top'] = 5.0  # Change max on axis (example, see https://climrisk.org/emberfactory/doc/parameters)

# Add ember to graph (creating an ember group automatically):
egr.add(be)

# Actually produce the diagram
outfile = egr.draw()
