"""
Purpose: test the drawing of a different axis for each group of embers
"""
from embermaker.embergraph import EmberGraph
from embermaker.ember import EmberGroup
from embermaker import ember as emb
from copy import deepcopy

# Create an ember graph
outfile = 'examples/out/test_multiple'
egr = EmberGraph(outfile, grformat="PDF")
egr.gp['haz_axis_top'] = 5.0  # Change max on axis (example, see https://climrisk.org/emberfactory/doc/parameters)
egr.set_yaxis(bottom=0, top=5, var_name_std='GMT', name="Global mean temperature increase", var_unit="°C",
              lines_num=10)

# Create an embegroup
gbe = EmberGroup("Group 1")

# Create 1st ember and add transitions
be1 = emb.Ember(name='Ember 1', haz_valid=[0, 5], haz_name_std='GMT')
be1.trans_create(name='undetectable to moderate', tmin=1, tmedian=1.7, tmax=2.0, confidence='very high')
be1.trans_create(name='moderate to high', tmin=2.0, tmax=3.7, confidence='medium')
gbe.add(be1)

# Create 2nd ember and add transitions
be = emb.Ember(name='Ember 2', haz_valid=[0, 5], haz_name_std='GMT')
be.trans_create(name='undetectable to moderate', tmin=0.8, tmax=1.2, confidence='very high')
be.trans_create(name='moderate to high', tmin=1.5, tmax=2.0, confidence='medium')
be.trans_create(name='high to very high', tmin=2.0, tmax=5.0, confidence='medium')
gbe.add(be)

# Add embergroup to graph
egr.add(gbe)

# Create 2nd embegroup
gbe = EmberGroup("Group 2")

# Create ember 3 and add transitions
be = emb.Ember(name='Ember 3', haz_valid=[0, 4], haz_name_std='GMSST')
be.trans_create(name='undetectable to moderate', tmin=0.6, tmedian=1.1, tmax=1.4, confidence='medium')
be.trans_create(name='moderate to high', tmin=1.8, tmax=4.7, confidence='low')
gbe.add(be)

# Create ember 4 and add transitions
be = emb.Ember(name='Ember 4', haz_valid=[0, 4], haz_name_std='GMSST')
be.trans_create(name='undetectable to moderate', tmin=0.3, tmax=1.0, confidence='very high')
be.trans_create(name='moderate to high', tmin=1.5, tmax=2.0, confidence='medium')
be.trans_create(name='high to very high', tmin=2.0, tmax=5.0, confidence='medium')
gbe.add(be)

# Add ember 1 again, in group 2
# An ember cannot be added twice, so a deepcopy is needed.
# This is theoretical case, which should rarely occur, or possibly never, hence it is not dealt with within the library
be1_copy = deepcopy(be1)
be1_copy.name = ("Copy of Ember1")
gbe.add(be1_copy)

# Add embergroup to graph
gbe.set_yaxis(bottom=0, top=4, var_unit="°C", var_name_std='GMSST', name='Global mean sea-surface temperature')
# Embers 3 and 4 will not require a conversion, but the copy of Ember1 will require one (GMT=>GMSST), as a test.
egr.add(gbe)

# Actually produce the diagram
outfile = egr.draw()
